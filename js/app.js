var abstractApp = angular.module('abstractApp', [
  'ngRoute',
  'abstractControllers'
]);

abstractApp.config(['$routeProvider','$httpProvider',
  function($routeProvider, $httpProvider) {
    $routeProvider.
      when('/main', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      }).
      when('/thanks', {
      	templateUrl: 'views/thanks.html',
      	controller: 'ThanksCtrl'
      }).
      when('/contact',{
      	templateUrl: 'views/contact.html',
      	controller: 'ContactCtrl'
      }).
      otherwise({
        redirectTo: '/main'
      });

      $httpProvider.defaults.useXDomain = true;
$httpProvider.defaults.withCredentials = true;
  }]);

