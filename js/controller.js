var abstractControllers = angular.module('abstractControllers', []);

abstractControllers.controller('MainCtrl', ['$scope', '$http',
	function ($scope, $http) {
		var stArray = ['is changing development in...', 
						'is revolutionizing development in...',
						'will be stopping by around...',
						'is stealing Adobe\'s profits in...',
						'is making Visual Studio tremble in...'];

		$scope.subtitle = stArray[Math.floor(Math.random() * stArray.length)];

		var target_date = new Date("June 21, 2015").getTime();


		$scope.days = 0;
		$scope.hours = 0;
		$scope.minutes = 0;
		$scope.seconds = 0;

		setInterval(function () {
			var current_date = new Date().getTime();
			var seconds_left = (target_date - current_date) / 1000;

		    $scope.days = parseInt(seconds_left / 86400);
			seconds_left = seconds_left % 86400;
			 
			$scope.hours = parseInt(seconds_left / 3600);
			seconds_left = seconds_left % 3600;
			  
			$scope.minutes = parseInt(seconds_left / 60);
			$scope.seconds = parseInt(seconds_left % 60);

			$scope.$apply();
		}, 1000);

		

	}
])
.controller('ThanksCtrl', ['$scope', '$http',
	function ($scope, $http) {
	}
])
.controller('ContactCtrl', ['$scope', '$http',
	function ($scope, $http) {
	}
])
.controller('FooterCtrl', ['$scope', '$http',
	function ($scope, $http) {

		$scope.addToList = function(email){
			console.log("Signing Up For Newsletter. Keith will be pleased.");
			$http({url: 'http://XXXXXXXX.list-manage2.com/subscribe/post-json?c=?',
				method: "GET", 
				data: JSON.stringify({u:'e3aa63b46538ee1d8822b5f3bdd17c71-us10', 
									  id: '5353', email: email, double_optin: false, MERGE1: true}), 
					headers: {'Content-Type': 'application/json'} }).
			  success(function(data, status, headers, config) {

			  	window.location.href = "/#/thanks";
			  }).
			  error(function(data, status, headers, config) {
			  	console.log("ERROR: "+data);
			  });
		}
	}
]);